﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //connect to exchange service
                ExchangeService myService = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
                myService.Credentials = new WebCredentials("sb4p07@soton.ac.uk", "password");
                myService.AutodiscoverUrl("sb4p07@soton.ac.uk");

		//filters below set a time range to select emails - rember to change dates and Recieved/Sent accordingly                
		List<SearchFilter> filterList = new List<SearchFilter>();
                filterList.Add(new SearchFilter.IsLessThan(ItemSchema.DateTimeReceived, new DateTime(2014, 1, 1)));
                filterList.Add(new SearchFilter.IsGreaterThan(ItemSchema.DateTimeReceived, new DateTime(2012, 12, 31)));
                SearchFilter myFilter = new SearchFilter.SearchFilterCollection(LogicalOperator.And, filterList.ToArray());


                ItemView myView = new ItemView(100000);

		
		//ordering of results - remeber to chage Recieved/Sent accordingly
                myView.PropertySet = new PropertySet(BasePropertySet.IdOnly, ItemSchema.Subject, ItemSchema.DateTimeReceived);
                myView.OrderBy.Add(ItemSchema.DateTimeReceived, SortDirection.Descending);

                FindItemsResults<Item> foundEmails = myService.FindItems(WellKnownFolderName.Inbox, myFilter,myView);

                FolderView fView = new FolderView(100);
                fView.PropertySet = new PropertySet(BasePropertySet.IdOnly);
                fView.PropertySet.Add(FolderSchema.DisplayName);
                fView.Traversal = FolderTraversal.Deep;
                FindFoldersResults foundFolders = myService.FindFolders(WellKnownFolderName.ArchiveRoot, fView);

                
		//to find a custom named archive folder - remember to change string accordingly
		FolderId fiD = new FolderId("dummy");
                foreach (Folder theFolder in foundFolders.Folders)
                {
                    if (theFolder.DisplayName.Contains("inbox2013"))
                    {
                        fiD = theFolder.Id;
                    }
                    


                }


                foreach (Item email in foundEmails.Items)
                {
                    email.Move(fiD);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
                Console.ReadLine();
            }
        }
    }
}
